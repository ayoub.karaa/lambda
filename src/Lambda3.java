
@FunctionalInterface
interface Arithmetic{
    int operation(int a, int b);
}

public class Lambda3 {
    public static void main (String []args){
        Arithmetic addition = (a,b)->(a+b);
        System.out.println("addition : "+addition.operation(10,20));
        Arithmetic substraction = (a,b)->(a-b);
        System.out.println("substraction : "+substraction.operation(60,20));
        Arithmetic division = (a,b)->(a/b);
        System.out.println("division : "+division.operation(40,20));

    }
}
