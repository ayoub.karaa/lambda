import java.sql.SQLOutput;

interface Drawable{
    void drow();
}
class DrawableImpl implements Drawable{

    @Override
    public void drow() {
        System.out.println("drow normal");
    }

}

interface  Color{
    void drowTow(String message);

}
interface Multiple{
    int drowThree(int a,int b);

}

public class Lambda1 {
    public static void main (String[]args){
        //Implementation basique d'une interface
        Drawable drawable = new DrawableImpl();
        drawable.drow();
        //Implementation anonime d'une interface
        Drawable drawable1= new Drawable() {
            @Override
            public void drow() {
                System.out.println("drow anonyme ");
            }
        };
        drawable1.drow();
        //Implementation using lambda
        Drawable drawable2 = () -> System.out.println("drow lambda");
        drawable2.drow();

        //Implement using lambda with PARAM
        Color color =  message -> System.out.println(message);
        color.drowTow("color with 1 param lambda");

        //Implement using lambda with multiple params
        Multiple multiple = (a,b)-> {return  (a + b);};
        System.out.println("summ is : "+ multiple.drowThree(2,5));
    }
}
