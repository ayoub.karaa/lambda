import javafx.print.Collation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortList {


    public static void main(String[] args){
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(30);
        list.add(20);
        list.add(50);
        list.add(40);
        System.out.println("start list");
        System.out.println(list);
        Collections.sort(list);
        System.out.println("sorted list with Collections");
        System.out.println(list);

        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1,"ayoub",24,1700));
        employees.add(new Employee(2,"yassin",23,1700));
        employees.add(new Employee(1,"hedi",27,2500));
        employees.add(new Employee(1,"majd",23,2100));
        Collections.sort(employees,new MySort());
        System.out.println(employees);
        System.out.println("using anonimous");
        Collections.sort(employees, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return (int) (o2.getSalary() - o1.getSalary()) ;
            }
        });
        System.out.println(employees);
        System.out.println("using lambda with name");
        Collections.sort(employees,( o3,o4) ->(int) (o4.getName().compareTo(o3.getName())));
        System.out.println(employees);

    }
}
class MySort implements Comparator<Employee>{

    @Override
    public int compare(Employee o1, Employee o2) {
        return (int) (o1.getSalary() - o2.getSalary()) ;
    }
}