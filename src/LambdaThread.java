public class LambdaThread {
    public static void main(String[]args){
        Thread  thread = new Thread( () -> System.out.println("thread : "+Thread.currentThread().getName()));
        thread.start(); 
    }

}



class MyThread implements Runnable{

    @Override
    public void run() {
        System.out.println("thread name => "+Thread.currentThread().getName());
    }
}